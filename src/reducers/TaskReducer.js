import { uuid } from "uuidv4";

const TaskReducer = (state, action) => {
  let newTasks = [];

  switch (action.type) {
    case "ADD_TASK":
      newTasks = [
        ...state.tasks,
        {
          id: uuid(),
          title: action.title,
          completed: action.completed,
        },
      ];

      if (action.title.length > 0) {
        localStorage.setItem(
          "tasks",
          JSON.stringify(newTasks)
        );

        return { ...state, tasks: newTasks };
      }
    case "REMOVE_TASK":
      newTasks = state.tasks;
      let filterRemovedTasks = newTasks.filter(
        (task) => task.id !== action.id
      );
      localStorage.setItem(
        "tasks",
        JSON.stringify(newTasks)
      );
      return {
        ...state,
        tasks: filterRemovedTasks,
      };

    case "COMPLETE_CHECKBOX":
      newTasks = state.tasks;
      let itemIndex = newTasks.findIndex(
        (task) => task.id === action.id
      );
      newTasks[itemIndex].completed = !newTasks[itemIndex]
        .completed;
      localStorage.setItem(
        "tasks",
        JSON.stringify(newTasks)
      );
      return {
        ...state,
        tasks: newTasks,
      };

    case "SEARCH_TASK":
      return {
        ...state,
        query: action.query,
      };

    case "SHOW_COMPLETED":
      return {
        ...state,
        filter: "COMPLETED",
      };

    case "SHOW_UNCOMPLETED":
      return {
        ...state,
        filter: "UNCOMPLETED",
      };

    case "SHOW_ALL":
      return {
        ...state,
        filter: "ALL",
      };

    default:
      break;
  }
};

export default TaskReducer;
