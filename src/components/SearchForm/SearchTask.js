import React, { useContext, useRef } from "react";
import TaskContext from "./../../contexts/TaskContext";
const SearchTask = () => {
  const { dispatchTask } = useContext(TaskContext);
  const inputSearch = useRef();

  const handleSearch = (e) => {
    e.preventDefault();
    dispatchTask({
      type: "SEARCH_TASK",
      query: inputSearch.current.value,
    });
  };

  return (
    <form
      className="todo"
      autoComplete="off"
      onSubmit={handleSearch}
    >
      <input type="text" ref={inputSearch} />
      <button type="submit">Search</button>
    </form>
  );
};

export default SearchTask;
