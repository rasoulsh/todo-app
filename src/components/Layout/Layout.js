import React, { useState, useReducer } from "react";
import Footer from "../Footer/Footer";
import SearchTask from "./../SearchForm/SearchTask";
import AddTask from "./../AddTask/AddTask";
import TaskList from "../TaskList/TaskList";
import TaskContext from "./../../contexts/TaskContext";
import TaskReducer from "./../../reducers/TaskReducer";

const Layout = () => {
  let tasks = JSON.parse(localStorage.getItem("tasks"));

  const [state, dispatch] = useReducer(TaskReducer, {
    tasks: tasks || [],
    query: "",
  });
  return (
    <TaskContext.Provider
      value={{
        tasks: state.tasks,
        filter: state.filter,
        dispatchTask: dispatch,
        query: state.query,
      }}
    >
      <div className="todo-list">
        <SearchTask />
        <AddTask />
        <TaskList />
        <Footer />
      </div>
    </TaskContext.Provider>
  );
};

export default Layout;
