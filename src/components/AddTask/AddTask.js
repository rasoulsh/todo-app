import React, {
  useRef,
  useContext,
  useEffect,
} from "react";
import TaskContext from "./../../contexts/TaskContext";
const AddTask = () => {
  const inputTitleRef = useRef();
  const { dispatchTask } = useContext(TaskContext);

  const handleAddTask = (e) => {
    e.preventDefault();
    dispatchTask({
      type: "ADD_TASK",
      title: inputTitleRef.current.value,
      completed: false,
    });
    inputTitleRef.current.value = "";
  };

  useEffect(() => {
    inputTitleRef.current.focus();
  }, []);

  return (
    <form
      className="todo"
      autoComplete="off"
      onSubmit={handleAddTask}
    >
      <input type="text" name="item" ref={inputTitleRef} />
      <button type="submit">+ Add To Your List</button>
    </form>
  );
};

export default AddTask;
