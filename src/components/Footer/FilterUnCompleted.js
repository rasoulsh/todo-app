import React, {
  useContext,
  useRef,
  useCallback,
} from "react";
import TaskContext from "./../../contexts/TaskContext";

const FilterUnCompleted = () => {
  const { dispatchTask } = useContext(TaskContext);
  const showUnCompletedCheckboxRef = useRef();

  const handleSort = useCallback(() => {
    showUnCompletedCheckboxRef.current.checked === true
      ? dispatchTask({ type: "SHOW_UNCOMPLETED" })
      : dispatchTask({ type: "SHOW_ALL" });
  }, [showUnCompletedCheckboxRef, dispatchTask]);

  return (
    <li className="todo-item">
      <input
        type="checkbox"
        ref={showUnCompletedCheckboxRef}
        onChange={handleSort}
      />
      <span className="itemName">UnCompleted Tasks</span>
    </li>
  );
};

export default FilterUnCompleted;
