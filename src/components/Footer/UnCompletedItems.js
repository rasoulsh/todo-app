import React, {
  useState,
  useContext,
  useEffect,
} from "react";
import TaskContext from "../../contexts/TaskContext";
const UnCompletedItems = () => {
  const { tasks } = useContext(TaskContext);
  const [availables, setAvailables] = useState(0);

  useEffect(() => {
    const availableTasks = tasks.filter(
      (task) => task.completed === false
    );
    setAvailables(availableTasks.length);
  }, [tasks]);

  return (
    <div>
      <span className="availables">{availables}</span>
      <span>Items Left</span>
    </div>
  );
};

export default UnCompletedItems;
