import React from "react";
import UnCompletedItems from "./UnCompletedItems";
import FilterCompleted from "./FilterCompleted";
import FilterUnCompleted from "./FilterUnCompleted";

const Footer = () => {
  return (
    <div className="todo-item counter">
      <UnCompletedItems />
      <ul>
        <FilterCompleted />
        <FilterUnCompleted />
      </ul>
    </div>
  );
};
export default Footer;
