import React, {
  useContext,
  useRef,
  useCallback,
} from "react";
import TaskContext from "./../../contexts/TaskContext";

const FilterCompleted = () => {
  const { dispatchTask } = useContext(TaskContext);
  const showCompletedCheckboxRef = useRef();

  const handleSort = useCallback(() => {
    showCompletedCheckboxRef.current.checked === true
      ? dispatchTask({ type: "SHOW_COMPLETED" })
      : dispatchTask({ type: "SHOW_ALL" });
  }, [showCompletedCheckboxRef, dispatchTask]);

  return (
    <li className="todo-item">
      <input
        type="checkbox"
        ref={showCompletedCheckboxRef}
        onChange={handleSort}
      />
      <span className="itemName">Completed Tasks</span>
    </li>
  );
};

export default FilterCompleted;
