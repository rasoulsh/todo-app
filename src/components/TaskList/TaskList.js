import React, { useContext } from "react";
import TaskContext from "./../../contexts/TaskContext";
import TaskItem from "./TaskItem";
const TaskList = () => {
  const { tasks, filter, query } = useContext(TaskContext);
  let filteredTasks = tasks;

  if (filter === "COMPLETED") {
    filteredTasks = tasks.filter((task) => task.completed);
  }
  if (filter === "UNCOMPLETED") {
    filteredTasks = tasks.filter(
      (task) => task.completed === false
    );
  }

  if (query) {
    filteredTasks.filter((task) =>
      task.title.includes(query)
    );
  }

  return (
    <ul className="list">
      {filteredTasks &&
        filteredTasks.map((task, index) => (
          <TaskItem
            key={index}
            index={index}
            completed={task.completed}
            id={task.id}
          >
            {task.title}
          </TaskItem>
        ))}
    </ul>
  );
};

export default TaskList;
