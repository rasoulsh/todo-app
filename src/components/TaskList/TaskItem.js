import React, {
  useContext,
  useEffect,
  useCallback,
} from "react";
import TaskContext from "./../../contexts/TaskContext";

const TaskItem = ({ children, index, completed, id }) => {
  const { tasks, dispatchTask } = useContext(TaskContext);

  const handleRemoveTask = (id) => {
    dispatchTask({
      type: "REMOVE_TASK",
      id,
    });
  };

  const handleChangeCompleteCheckbox = useCallback(() => {
    dispatchTask({
      type: "COMPLETE_CHECKBOX",
      id: id,
    });
  }, [dispatchTask, tasks]);

  return (
    <li className="todo-item">
      <input
        type="checkbox"
        defaultChecked={completed ? true : false}
        onChange={handleChangeCompleteCheckbox}
      />
      <span className="itemName">{children}</span>
      <button onClick={() => handleRemoveTask(id)}>
        ×
      </button>
    </li>
  );
};
export default TaskItem;
